﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionCliente
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class InicioSesion : Window
    {
        public InicioSesion()
        {
            InitializeComponent();
        }

        private void clicRegistrar(object sender, RoutedEventArgs e)
        {
            RegistroUsuario pantallaRegistroUsuario = new RegistroUsuario();
            pantallaRegistroUsuario.Show();
            this.Close();
        }

        private void clicIniciar(object sender, RoutedEventArgs e)
        {
            PaginaPrincipal pantallaPrincipal = new PaginaPrincipal();
            pantallaPrincipal.Show();
            this.Close();
        }
    }
}
