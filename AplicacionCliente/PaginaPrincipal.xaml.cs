﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplicacionCliente
{
    /// <summary>
    /// Lógica de interacción para PaginaPrincipal.xaml
    /// </summary>
    public partial class PaginaPrincipal : Window
    {
        public PaginaPrincipal()
        {
            InitializeComponent();
        }

        private void clicPerfil(object sender, RoutedEventArgs e)
        {
            Perfil pantallaPerfil = new Perfil();
            pantallaPerfil.Show();
            this.Close();
        }

        private void clicNuevaPartida(object sender, RoutedEventArgs e)
        {
            CreacionPartida pantallaCreacionPartida = new CreacionPartida();
            pantallaCreacionPartida.Show();
            this.Close();
        }

        private void clicEntrarPartida(object sender, RoutedEventArgs e)
        {
            PantallaAdivinador pantallaAdivinador = new PantallaAdivinador();
            pantallaAdivinador.Show();
            this.Close();
        }
    }
}
